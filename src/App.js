import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import './AppResponsive.css';
import Sidebar from './Pages/Sidebar'
import MyDashboard from './Pages/MyDashboard'
import ChainedDocument from './Pages/ChainedDocument'
import I2CReader from './Pages/I2CReader'
import MailInbox from './Pages/MailInbox'
import MailSent from './Pages/MailSent'
import MailDraft from './Pages/MailDraft'
import MailStared from './Pages/MailStared'
import Settings from './Pages/Settings'
import Help from './Pages/Help'



function App() {
  return (
    <section className="dashboard">
      <div className='innerboard'>        
          <Sidebar />
          <Routes>
            <Route exact path="/" element={<MyDashboard />} />  
            <Route exact path="/myDashboard" element={<MyDashboard />} />  
            <Route exact path="/chainedocument" element={<ChainedDocument />} />  
            <Route exact path="/i2CReader" element={<I2CReader />} />  
            <Route exact path="/mail/mailInbox" element={<MailInbox />} />  
            <Route exact path="/mail/mailSent" element={<MailSent />} />  
            <Route exact path="/mail/mailDraft" element={<MailDraft />} />  
            <Route exact path="/mail/mailStared" element={<MailStared />} />  
            <Route exact path="/settings" element={<Settings />} />  
            <Route exact path="/help" element={<Help />} />  
          </Routes>           
      </div>
    </section>
  );
}

export default App;
