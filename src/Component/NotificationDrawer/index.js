import React, { useEffect, useState } from 'react';
import { ImagePath } from '../../ImagePath';
import { Link, NavLink } from 'react-router-dom';
import 'react-perfect-scrollbar/dist/css/styles.css';
import PerfectScrollbar from 'react-perfect-scrollbar'

const NotificationDrawer = props => {

const { drawertoogle } = props;
const [notificationDrawer, setNotificationDrawer] = useState(false); 

useEffect(() => {
    setNotificationDrawer(drawertoogle);
}, [props]);


  return (    
        <div className={notificationDrawer ? "notification-sidebar opened" : "notification-sidebar closed"}>
            <div className='searchbar-wrap d-flex justify-content-end'>                   
                <div className='notification-box d-flex pr-5'>
                <Link to="#" className='bell'> <span className='dot'></span> <img src={ImagePath.BellIcon} className="img-fluid" alt="icon" /></Link>
                <span>Aug 20</span>
                </div>
            </div>
            <div className="d-flex justify-content-between align-items-center nhead">
                <h4>Notifications</h4>
                <Link to="#">Mark all as read</Link>
            </div>  
            <PerfectScrollbar className='nscroll'>
                <ul className="notification-lists list-unstyled">
                    <li>
                        <Link to="#" className="notification-media">
                            <div className="media-img">
                            <img src={ImagePath.AvtarImg} className="img-fluid" alt="icon" />
                            </div>
                            <div className="media-body position-relative">
                            <span className="ndate">2 min ago</span>
                                <h6>Nelly Miller</h6>
                                <p className="text-truncate">Shared File Name.i2C with someone.</p>
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link to="#" className="notification-media">
                            <div className="media-img">
                            <img src={ImagePath.AvtarImg} className="img-fluid" alt="icon" />
                            </div>
                            <div className="media-body position-relative">
                            <span className="ndate">2 min ago</span>
                                <h6>Nelly Miller</h6>
                                <p className="text-truncate">Shared File Name.i2C with someone.</p>
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link to="#" className="notification-media">
                            <div className="media-img">
                            <img src={ImagePath.AvtarImg} className="img-fluid" alt="icon" />
                            </div>
                            <div className="media-body position-relative">
                            <span className="ndate">2 min ago</span>
                                <h6>Nelly Miller</h6>
                                <p className="text-truncate">Shared File Name.i2C with someone.</p>
                            </div>
                        </Link>
                    </li>
                
                </ul>
            </PerfectScrollbar>
        </div>
  );
}

export default NotificationDrawer;
