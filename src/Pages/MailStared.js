import React from 'react';
import { MDBTable, MDBTableBody, MDBTableHead,  MDBProgress } from 'mdbreact';
import 'react-perfect-scrollbar/dist/css/styles.css';
import PerfectScrollbar from 'react-perfect-scrollbar'
import { ImagePath } from '../ImagePath';
import { Link, NavLink } from 'react-router-dom';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, UncontrolledDropdown } from 'reactstrap';
import Divider from "@material-ui/core/Divider";
import Button from 'reactstrap/lib/Button';


function MailStared() {
  return (
        <section className='rightbar-wrap no-padding mt-0'>
            <div className='mailbody-box w-100 h-100'>
                <div className='mail-list-box'>
                    <div className='d-flex top-fileterbar w-100 justify-content-between align-items-center'>
                        <div className='clerfix'>
                            <h5 className='mb-0'>Chained Starred</h5>
                        </div>
                        <div className='sort-wrap'>
                            <UncontrolledDropdown setActiveFromChild className="custom">
                                <DropdownToggle tag="a" className="nav-link" caret>
                                    <Link to="#" className='link'>Filter</Link>
                                </DropdownToggle>
                                <DropdownMenu right className="custom">
                                    <DropdownItem tag="a" href="/blah" >Name</DropdownItem>
                                    <DropdownItem tag="a" href="/blah" >Date</DropdownItem>
                                    <DropdownItem tag="a" href="/blah" >File Type</DropdownItem>
                                    <DropdownItem tag="a" href="/blah" >Classification</DropdownItem>
                                    <Divider className='my-2' />
                                    <DropdownItem tag="a" href="/blah" >Ascending</DropdownItem>
                                    <DropdownItem tag="a" href="/blah" >Descending</DropdownItem>
                                </DropdownMenu>
                                </UncontrolledDropdown>
                        </div>
                    </div>
                    <PerfectScrollbar>
                        <div className='in-listbox'>
                            <div className='day-strip'>
                                Yesterday 
                            </div>

                            <div className='maillist media unread'>
                                <div className='media-img'>
                                    <img src={ImagePath.MailLogo1} className="img-fluid" alt="icon" />
                                </div>
                                <div className='media-body'>
                                    <Link to="" className='starmail'> <img src={ImagePath.StarFill} className="img-fluid" alt="icon" /></Link>
                                    <h6>Citizen Bank </h6>
                                    <div className='d-flex justify-content-between'>
                                        <span>Loan Application </span>
                                        <span className='timemail'>Tue 2:35 AM</span>
                                    </div>                                   
                                    <p className='text-truncate'>Hi Frank, hope all is well. Please fill fgdfhgghgf</p>
                                </div>
                            </div>
                            <div className='maillist media read'>
                                <div className='media-img'>
                                    <img src={ImagePath.MailLogo1} className="img-fluid" alt="icon" />
                                </div>
                                <div className='media-body'>
                                    <Link to="" className='starmail'> <img src={ImagePath.StarFill} className="img-fluid" alt="icon" /></Link>
                                    <h6>Citizen Bank Information </h6>
                                    <div className='d-flex justify-content-between'>
                                        <span>SSN needed- Reminder</span>
                                        <span className='timemail'>Tue 2:35 AM</span>
                                    </div>                                   
                                    <p className='text-truncate'>Hi Frank, hope all is well. Please fill fgdfhgghgf</p>
                                </div>
                            </div>
                            <div className='maillist media read'>
                                <div className='media-img'>
                                    <img src={ImagePath.MailLogo2} className="img-fluid" alt="icon" />
                                </div>
                                <div className='media-body'>
                                    <Link to="" className='starmail'> <img src={ImagePath.StarFill} className="img-fluid" alt="icon" /></Link>
                                    <h6>El Comino Hospital </h6>
                                    <div className='d-flex justify-content-between'>
                                        <span>Healthcare record </span>
                                        <span className='timemail'>Tue 2:35 AM</span>
                                    </div>                                   
                                    <p className='text-truncate'>Hi Frank, hope all is well. Please fill fgdfhgghgf</p>
                                </div>
                            </div>
                        </div>                        
                    </PerfectScrollbar>
                </div>
                {/* end of mail list */}
                <div className='mail-details-box position-relative'>
                    <div className='mail-header-box mb-4 mb-md-5'>
                        <div className='mail-avtar-box'>
                            <div className='media-img'>
                                <img src={ImagePath.MailLogo1} className="img-fluid" alt="icon" />
                            </div>
                            <div className='media-body'>
                                <h6 className="mb-0">Financial Department</h6>
                                <div className="d-flex justify-content-between">
                                    <div className="w-50 pr-md-4">
                                        <span>frank.garrett@gmail.com</span>
                                    </div>
                                    <div className="w-50 mail-date-format">
                                        <div className="text-left pr-3">
                                            <span>March 26, 2021, 2:24PM</span>
                                        </div>
                                        <div className="m-forword d-flex">
                                            <Button type="button" className="star-fill-btn"></Button>
                                            <Button type="button" className="forward-btn"></Button>

                                        <UncontrolledDropdown setActiveFromChild className="custom">
                                            <DropdownToggle tag="a" className="nav-link" >
                                                <Link to="#" className='angle-link'></Link>
                                            </DropdownToggle>
                                            <DropdownMenu right className="custom">
                                                <DropdownItem tag="a" href="/blah" >Name</DropdownItem>
                                                <DropdownItem tag="a" href="/blah" >Date</DropdownItem>
                                                <DropdownItem tag="a" href="/blah" >File Type</DropdownItem>
                                                <DropdownItem tag="a" href="/blah" >Classification</DropdownItem>
                                                <Divider className='my-2' />
                                                <DropdownItem tag="a" href="/blah" >Ascending</DropdownItem>
                                                <DropdownItem tag="a" href="/blah" >Descending</DropdownItem>
                                            </DropdownMenu>
                                            </UncontrolledDropdown>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div className="inboxbody-box">
                        <div className="body-hrmail border-bottom mb-4">
                            <h2 className="heading">Loan application </h2>
                        </div>

                        <div className="mail-paragraph pb-3">
                            <p>Hi Aida, </p>
                            <br />
                            <p>Please fill the attached  loan application , sign it and forward it with the following requirements to us. We need the following information from you:</p>
                            <br />
                            <p>1- Your full name</p>                           
                            <p>2- Your social security number</p>                             
                            <p>3- Your ID back and front scan</p>
                            <br />
                            <p>Please reply to this email with any question you might have.</p>
                            <br />
                            <p>Best,</p>
                            <p>Citizen Bank Financial Department</p>
                        </div>

                        <div className="mailattach-box mt-5 pt-2 border-top">
                            <div className="d-flex justify-content-between align-items-center">
                                <h6 className="mb-0">2 Attachments</h6>
                                <Button type="button" className="download-btn"></Button>
                            </div>

                            <ul className="attached-box list-unstyled">
                                <li>
                                    <div className="docbox position-relative">
                                        <div className="overlay-attachment">                                        
                                            <Button type="button" className=" eye-btn"></Button>
                                            <Button type="button" className="download-btn-white"></Button>
                                        </div>
                                        {/* end of overlay */}
                                        <img src={ImagePath.DemoImg} className="img-fluid" alt="icon" />
                                        <small className="filename-doc">Loan app...pdf.i2c</small>
                                        <small className="filename-doc">1KB</small>
                                    </div>
                                </li>
                                <li>
                                    <div className="docbox position-relative">                                      
                                        <div className="overlay-attachment">                                        
                                            <Button type="button" className=" eye-btn"></Button>
                                            <Button type="button" className="download-btn-white"></Button>
                                        </div>
                                        {/* end of overlay */}
                                        <img src={ImagePath.DemoImg} className="img-fluid" alt="icon" />
                                        <small className="filename-doc">Loan app...pdf.i2c</small>
                                        <small className="filename-doc">1KB</small>
                                    </div>
                                </li>
                                <li>
                                    <div className="docbox position-relative">
                                        <Button type="button" className="close-btn"></Button>
                                        <img src={ImagePath.DemoImg} className="img-fluid" alt="icon" />
                                        <small className="filename-doc">Loan app...pdf.i2c</small>
                                        <small className="filename-doc">1KB</small>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        {/* end of attachment-div */}

                        <div className="mailreply-box">
                            <p className="mb-0">Click here to <Link to="#">Reply</Link> or <Link to="#">Forward</Link></p>
                        </div>
                    </div>
                </div>
                {/* end of mail details */}
            </div>
          </section>
  );
}

export default MailStared;

