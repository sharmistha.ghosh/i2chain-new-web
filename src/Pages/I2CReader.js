import React from 'react';
import { MDBTable, MDBTableBody, MDBTableHead,  MDBProgress } from 'mdbreact';
import { ImagePath } from '../ImagePath';
import { Link, NavLink } from 'react-router-dom';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, UncontrolledDropdown, ModalBody } from 'reactstrap';

function I2CReader() {
  return (
        <section className='rightbar-wrap'>
            <div className='center-part w-100'>
              <div className='d-flex justify-content-between w-100 align-items-center mt-5'>
                <h2 className='heading mb-0'>View your i2C file</h2>
              </div>
                
              <div className='file-open-white'>
                <text className='text-first'>Drag and Drop i2C file here</text>
                <text className='text-second'>File Supported: i2C file</text>
                <button className='btn button-file-choose'>Choose File</button>
              </div>
                               
            </div>
          </section>
  );
}

export default I2CReader;
