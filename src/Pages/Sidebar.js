import React, { useState } from 'react';
import 'react-perfect-scrollbar/dist/css/styles.css';
import PerfectScrollbar from 'react-perfect-scrollbar'
import { ImagePath } from '../ImagePath';
import { Link, NavLink } from 'react-router-dom';


function Sidebar() {
  const [mail, setMail] = useState(false); 

  const openMailMenu = () => {
    console.log("openMailMenu===");
    setMail(!mail);
  }

  return (
    <aside className='d-sidebar-wrap'>
        <PerfectScrollbar>
          <div className='inner-dsidebar'>
            <Link to="/" className='nav-logo'><img src={ImagePath.Logo} className="img-fluid" alt="logo" /></Link>
            <div className='d-profile-wrap text-center'>
              <div className='avatar-box'>
                <img src={ImagePath.ProfileImage} className="w-100 h-100" alt="profile" />
              </div>
              <Link to="/profile"><h4>Frank Garrettt</h4></Link>
              <h6>Admin</h6>
              <div className='pt-3'>
                <button type='button' className='theme-btn btn btn-block'>+ Chain Now!</button>
              </div>
            </div>
            <div className='sidebar-navigation'>
              <ul className='list-unstyled'>
              <li><NavLink exact to="/" activeClassName = "active-class"><img src={ImagePath.NavIcon1} className="img-fluid" alt="icon" /> Dashboard </NavLink></li>
                <li><NavLink exact to="/chainedocument" activeClassName = "active-class"><img src={ImagePath.NavIcon2} className="img-fluid" alt="icon" /> Chained Documents</NavLink></li>
                <li className='has-submenu'>
                  <div className="d-flex justify-content-between align-items-center">
                  <div onClick={openMailMenu} ><NavLink exact to="/mail/mailInbox" activeClassName = "active-class" ><img src={ImagePath.NavIcon3} className="img-fluid" alt="icon" />i2Mail</NavLink></div>
                    <button type="button" className="compose-btn"></button>
                  </div>
                  {mail &&                   
                  <div className='submenu-box'>
                    <NavLink exact to="/mail/mailInbox" activeClassName = "active-class" >Inbox</NavLink>
                    <NavLink exact to="/mail/mailSent"  activeClassName = "active-class" >Sent</NavLink>
                    <NavLink exact to="/mail/mailDraft"  activeClassName = "active-class" >Drafts</NavLink>
                    <NavLink exact to="/mail/mailStared" activeClassName = "active-class" >Starred</NavLink>
                  </div>   
                  }        
                </li>
                <li><NavLink exact to="/i2CReader" activeClassName = "active-class"><img src={ImagePath.NavIcon4} className="img-fluid" alt="icon" /> i2C Reader</NavLink></li>
                <li><NavLink exact to="/help" activeClassName = "active-class" ><img src={ImagePath.NavIcon5} className="img-fluid" alt="icon" /> Help</NavLink></li>
                <li><NavLink exact to="/settings" activeClassName = "active-class" ><img src={ImagePath.NavIcon6} className="img-fluid" alt="icon" /> Settings</NavLink></li>
              </ul>
            </div>

            <Link to="/" className='logout-nav'><img src={ImagePath.NavIcon7} className="img-fluid" alt="icon" /> Log out</Link>
          </div>
          </PerfectScrollbar>
        </aside>
  );
}

export default Sidebar;
