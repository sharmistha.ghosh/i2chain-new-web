import React from 'react';
import { MDBTable, MDBTableBody, MDBTableHead,  MDBProgress } from 'mdbreact';
import 'react-perfect-scrollbar/dist/css/styles.css';
import PerfectScrollbar from 'react-perfect-scrollbar'
import { ImagePath } from '../ImagePath';
import { Link, NavLink } from 'react-router-dom';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, UncontrolledDropdown } from 'reactstrap';
import Divider from "@material-ui/core/Divider";
import Button from 'reactstrap/lib/Button';


function MailSent() {
  return (
        <section className='rightbar-wrap no-padding mt-0'>
            <div className='mailbody-box w-100 h-100'>
                <div className='mail-list-box'>
                    <div className='d-flex top-fileterbar w-100 justify-content-between align-items-center'>
                        <div className='clerfix'>
                            <h5 className='mb-0'>Chained Sent</h5>
                        </div>
                        <div className='sort-wrap'>
                            <UncontrolledDropdown setActiveFromChild className="custom">
                                <DropdownToggle tag="a" className="nav-link" caret>
                                    <Link to="#" className='link'>Filter</Link>
                                </DropdownToggle>
                                <DropdownMenu right className="custom">
                                    <DropdownItem tag="a" href="/blah" >Name</DropdownItem>
                                    <DropdownItem tag="a" href="/blah" >Date</DropdownItem>
                                    <DropdownItem tag="a" href="/blah" >File Type</DropdownItem>
                                    <DropdownItem tag="a" href="/blah" >Classification</DropdownItem>
                                    <Divider className='my-2' />
                                    <DropdownItem tag="a" href="/blah" >Ascending</DropdownItem>
                                    <DropdownItem tag="a" href="/blah" >Descending</DropdownItem>
                                </DropdownMenu>
                                </UncontrolledDropdown>
                        </div>
                    </div>
                    <PerfectScrollbar>
                        <div className='in-listbox'>
                            <div className='day-strip'>
                                Yesterday 
                            </div>

                            <div className='maillist media unread'>
                                <div className='media-img'>
                                    <img src={ImagePath.MailLogo1} className="img-fluid" alt="icon" />
                                </div>
                                <div className='media-body'>
                                    <Link to="" className='starmail'> <img src={ImagePath.Star} className="img-fluid" alt="icon" /></Link>
                                    <h6>Citizen Bank </h6>
                                    <div className='d-flex justify-content-between'>
                                        <span>Loan Application </span>
                                        <span className='timemail'>Tue 2:35 AM</span>
                                    </div>                                   
                                    <p className='text-truncate'>Hi Frank, hope all is well. Please fill fgdfhgghgf</p>
                                </div>
                            </div>
                            <div className='maillist media read'>
                                <div className='media-img'>
                                    <img src={ImagePath.MailLogo1} className="img-fluid" alt="icon" />
                                </div>
                                <div className='media-body'>
                                    <Link to="" className='starmail'> <img src={ImagePath.Star} className="img-fluid" alt="icon" /></Link>
                                    <h6>Citizen Bank Information </h6>
                                    <div className='d-flex justify-content-between'>
                                        <span>Loan Application </span>
                                        <span className='timemail'>Tue 2:35 AM</span>
                                    </div>                                   
                                    <p className='text-truncate'>Hi Frank, hope all is well. Please fill fgdfhgghgf</p>
                                </div>
                            </div>
                            <div className='day-strip'>
                                Last Week
                            </div>
                            <div className='maillist media read'>
                                <div className='media-img'>
                                    <img src={ImagePath.MailLogo2} className="img-fluid" alt="icon" />
                                </div>
                                <div className='media-body'>
                                    <Link to="" className='starmail'> <img src={ImagePath.Star} className="img-fluid" alt="icon" /></Link>
                                    <h6>El Comino Hospital </h6>
                                    <div className='d-flex justify-content-between'>
                                        <span>Loan Application </span>
                                        <span className='timemail'>Tue 2:35 AM</span>
                                    </div>                                   
                                    <p className='text-truncate'>Hi Frank, hope all is well. Please fill fgdfhgghgf</p>
                                </div>
                            </div>
                        </div>                        
                    </PerfectScrollbar>
                </div>
                {/* end of mail list */}
                <div className='mail-details-box position-relative'>
                  <div className='inner-mail-area'>
                    <div className="inboxbody-box">
                        <div className="body-hrmail border-bottom mb-4">
                            <h2 className="heading">Loan application </h2>
                        </div>

                        <div className='body-hrmail border-bottom mb-4 pb-4'>
                          <div className='mail-avtar-box'>
                              <div className='media-img'>
                                  <img src={ImagePath.MailLogo1} className="img-fluid" alt="icon" />
                              </div>
                              <div className='media-body'>
                                  <div className="d-flex justify-content-between">
                                      <div className="text-left pt-2">
                                          <p>Hi Frank, Please fill the attached loan application</p>
                                      </div>
                                      <div className="w-50 mail-date-format">
                                          <div className="text-left pr-3">
                                              <span>March 26, 2021, 2:24PM</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            </div>
                        </div>

                        <div className='body-hrmail'>
                          <div className='mail-avtar-box'>
                              <div className='media-img'>
                                  <img src={ImagePath.ProfileImage} className="img-fluid" alt="icon" />
                              </div>
                              <div className='media-body'>
                                  <div className="d-flex justify-content-between">
                                      <div className="mail-paragraph">
                                          <p>Hello,</p>
                                          <p>Attached are the requested form.</p>
                                          <p>Best, Frank</p>
                                      </div>
                                      <div className="w-50 mail-date-format">
                                          <div className="text-left pr-3">
                                              <span>March 26, 2021, 2:24PM</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            </div>
                        </div>

                        <div className="mailattach-box mt-5 pt-2 border-top">
                            <div className="d-flex justify-content-between align-items-center">
                                <h6 className="mb-0">2 Attachments</h6>
                                <Button type="button" className="download-btn"></Button>
                            </div>

                            <ul className="attached-box list-unstyled">
                                <li>
                                    <div className="docbox position-relative">
                                        <Button type="button" className="dot-btn"></Button>
                                        <img src={ImagePath.DemoImg} className="img-fluid" alt="icon" />
                                        <small className="filename-doc">Loan app...pdf.i2c</small>
                                        <small className="filename-doc">1KB</small>
                                    </div>
                                </li>
                                <li>
                                    <div className="docbox position-relative">
                                        <Button type="button" className="dot-btn"></Button>
                                        <img src={ImagePath.DemoImg} className="img-fluid" alt="icon" />
                                        <small className="filename-doc">Account i...pdf.i2c</small>
                                        <small className="filename-doc">5KB</small>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    </div>
                {/* end of mail details */}
                  <div className="mailreply-box">
                      <p className="mb-0">Click here to <Link to="#">Reply</Link> or <Link to="#">Forward</Link></p>
                  </div>
                </div>



            </div>
          </section>
  );
}

export default MailSent;

