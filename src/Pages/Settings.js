import React from 'react';
import { MDBRow, MDBCol, } from 'mdbreact';
import 'react-perfect-scrollbar/dist/css/styles.css';
import PerfectScrollbar from 'react-perfect-scrollbar'
import { ImagePath } from '../ImagePath';
import { Link, NavLink } from 'react-router-dom';


function Settings() {
  return (
        <section className='rightbar-wrap no-padding mt-0'>
            <div className='mailbody-box w-100 h-100'>
                <div className='mail-list-box'>
                    <div className='top-fileterbar w-100 mb-1'>
                        <div className='clerfix'>
                            <span className='d-block'>Frank.Garrett@i2Chain.com</span>
                        </div>                      
                    </div>
                    <PerfectScrollbar>
                        <div className='in-listbox'>
                            <ul className='list-unstyled settings-ul'>
                                <li>
                                  <Link to="#" className='active'> <img src={ImagePath.SettingIcon1} className='img-fluid' alt="icon" /> <span>General Settings</span></Link>
                                </li>
                                <li>
                                  <Link to="#"> <img src={ImagePath.SettingIcon2} className='img-fluid' alt="icon" /> <span>Security & Login</span></Link>
                                </li>
                                <li>
                                  <Link to="#"> <img src={ImagePath.SettingIcon3} className='img-fluid' alt="icon" /> <span>Language & Region</span></Link>
                                </li>
                                <li>
                                  <Link to="#"> <img src={ImagePath.SettingIcon4} className='img-fluid' alt="icon" /> <span>Get Apps & Add-Ins</span></Link>
                                </li>
                                <li>
                                  <Link to="#"> <img src={ImagePath.SettingIcon5} className='img-fluid' alt="icon" /> <span>Administration</span></Link>
                                </li>
                                <li>
                                  <Link to="#"> <img src={ImagePath.SettingIcon6} className='img-fluid' alt="icon" /> <span>User Management</span></Link>
                                </li>
                                <li>
                                  <Link to="#"> <img src={ImagePath.SettingIcon7} className='img-fluid' alt="icon" /> <span>Brand Personalization</span></Link>
                                </li>                                
                            </ul>
                        </div>                        
                    </PerfectScrollbar>
                </div>
                {/* end of mail list */}
                <div className='settings-details-box position-relative'>
                    <div className='settings-header-wrap border-bottom px-4'>
                      <h2 className='heading'>General Settings</h2>
                    </div>
                    <div className='settings-body-wrap p-4'>
                    <div className='setting forms' id="genral-settings">
                          <div className=' border-bottom pb-3'>
                            <h6 className='b-heading'>Gender Pronoun</h6>
                                <MDBRow className='align-items-center'>
                                    <MDBCol sm={6}>
                                        <div className='form-group'>
                                          <label>Preferred Gender Pronoun </label>
                                        </div>
                                    </MDBCol>
                                    <MDBCol sm={6} className='pl-md-5'>
                                        <div className='form-group pl-md-5'>
                                        <select className="browser-default form-control">
                                            <option>He/Him/His</option>
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                          </select>
                                        </div>
                                    </MDBCol>
                                </MDBRow>                            
                          </div>

                          <div className='pb-2 pt-4 w-100'>
                            <h6 className='b-heading'>Basic Information </h6>
                          </div>
                          <MDBRow className='align-items-center'>
                              <MDBCol sm={6}>
                                  <div className='form-group'>
                                    <label>Photo</label>
                                  </div>
                              </MDBCol>
                              <MDBCol sm={6} className='pl-md-5'>
                                  <div className='form-group pl-md-5'>
                                   <div className='filetypebox p-1'>
                                     <small>Image format with max size of 3MB</small>
                                      <input type="file" id="uploadfile" hidden />
                                      <div className='thumbbox'>
                                        <img src={ImagePath.ProfileImage} className="w-100 h-100" alt="profile" />
                                      </div>
                                      <label className='uploadbtn theme-btn' for="uploadfile">Upload new photo</label>
                                    <button type='button' className='btn theme-outline-btn'>Remove</button>
                                   </div>
                                  </div>
                              </MDBCol>
                          </MDBRow>
                          <MDBRow className='align-items-center'>
                              <MDBCol sm={6}>
                                  <div className='form-group'>
                                    <label>Full Name </label>
                                  </div>
                              </MDBCol>
                              <MDBCol sm={6} className='pl-md-5'>
                                  <div className='form-group pl-md-5'>
                                    <input type="text"  className="form-control"  />
                                  </div>
                              </MDBCol>
                          </MDBRow>
                          <MDBRow className='align-items-center'>
                              <MDBCol sm={6}>
                                  <div className='form-group'>
                                    <label>Email Address </label>
                                  </div>
                              </MDBCol>
                              <MDBCol sm={6} className='pl-md-5'>
                                  <div className='form-group pl-md-5'>
                                    <input type="text"  className="form-control"  />
                                  </div>
                              </MDBCol>
                          </MDBRow>
                          <MDBRow className='align-items-center'>
                              <MDBCol sm={6}>
                                  <div className='form-group'>
                                    <label>Phone Number</label>
                                  </div>
                              </MDBCol>
                              <MDBCol sm={6} className='pl-md-5'>
                                  <div className='form-group pl-md-5'>
                                    <input type="text"  className="form-control"  />
                                  </div>
                              </MDBCol>
                          </MDBRow>
                          <MDBRow className='align-items-center'>
                              <MDBCol sm={6}>
                                  <div className='form-group'>
                                    <label>Your Occupation </label>
                                  </div>
                              </MDBCol>
                              <MDBCol sm={6} className='pl-md-5'>
                                  <div className='form-group pl-md-5'>
                                    <input type="text"  className="form-control"  />
                                  </div>
                              </MDBCol>
                          </MDBRow>
                          <MDBRow className='align-items-center'>
                              <MDBCol sm={6}>
                                  <div className='form-group'>
                                    <label>Your Role </label>
                                  </div>
                              </MDBCol>
                              <MDBCol sm={6} className='pl-md-5'>
                                  <div className='form-group pl-md-5'>
                                    <span className='btn theme-btn d-block rounded ml-md-5 m-0'>Admin</span>
                                  </div>
                              </MDBCol>
                          </MDBRow>
                          </div> 

                          {/* security  and login start 
                          <div className='setting forms' id="security&login">                           
                              <div className='form-group border-bottom py-2 position-relative'>
                                <div className='pr-5'>
                                  <h6 className='b-heading mb-0'>Change Password</h6>
                                  <label>Choose a unique password to protect your account </label>
                                </div>
                                <Link to="#" className='change-btn'>Change</Link>                                 
                              </div>
                              <div className='form-group border-bottom py-2 position-relative'>
                                <div className='pr-5'>
                                  <h6 className='b-heading mb-0'>Subscription</h6>
                                  <label>Your current plan is Freemium</label>
                                </div>
                                <Link to="#" className='change-btn'>Change</Link>                                 
                              </div>
                              <div className='form-group border-bottom py-2 d-flex justify-content-between'>
                                <div className='w-75'>
                                  <h6 className='b-heading mb-0'>Enable location services</h6>
                                </div>
                                <div className='w-25 d-flex justify-content-end'>
                                <div className='custom-control custom-switch'>
                                  <input  type='checkbox' className='custom-control-input' id='customSwitches'/>
                                  <label className='custom-control-label' htmlFor='customSwitches'></label>
                                </div>
                                </div>              
                              </div>
                            
                          </div>
                         security  and login end */}
                         
                    </div>
                    <div className='settings-footer-wrap px-4 border-top pt-4 mt-5'>
                          <div className='footer-btn d-flex'>
                              <button type='submit' className='btn theme-btn rounded px-4'>Update</button>
                              <button type='submit' className='btn outline-btn rounded px-4'>Discard</button>
                          </div>
                    </div>
                </div>
                {/* end of mail details */}
            </div>
          </section>
  );
}

export default Settings;
