import Logo from '../src/Assets/images/logo.svg';
import ProfileImage from '../src/Assets/images/profile-pic.png';
import NavIcon1 from '../src/Assets/images/icons/home-icon.svg';
import NavIcon2 from '../src/Assets/images/icons/document-icon.svg';
import NavIcon3 from '../src/Assets/images/icons/mail.svg';
import NavIcon4 from '../src/Assets/images/icons/i2c.svg';
import NavIcon5 from '../src/Assets/images/icons/help.svg';
import NavIcon6 from '../src/Assets/images/icons/setting.svg';
import NavIcon7 from '../src/Assets/images/icons/logout.svg';
import SearchIcon from '../src/Assets/images/icons/search.svg';
import BellIcon from '../src/Assets/images/icons/bell.svg';
import TipIcon from '../src/Assets/images/icons/tip.svg';
import MailLogo1 from '../src/Assets/images/icons/green-mail-logo.svg';
import MailLogo2 from '../src/Assets/images/icons/mail-logo2.svg';
import Star from '../src/Assets/images/icons/star.svg';
import StarFill from '../src/Assets/images/icons/star-fill.svg';
import DemoImg from '../src/Assets/images/icons/attachment-icon.svg';
import AvtarImg from '../src/Assets/images/avtar.svg';
import SettingIcon1 from '../src/Assets/images/icons/cog-icon.svg';
import SettingIcon2 from '../src/Assets/images/icons/lock-icon.svg';
import SettingIcon3 from '../src/Assets/images/icons/globe-icon.svg';
import SettingIcon4 from '../src/Assets/images/icons/box-icon.svg';
import SettingIcon5 from '../src/Assets/images/icons/brifcase-icon.svg';
import SettingIcon6 from '../src/Assets/images/icons/user-icon.svg';
import SettingIcon7 from '../src/Assets/images/icons/box-icon.svg';
import SettingIcon8 from '../src/Assets/images/icons/admin-user.svg';
import SettingIcon9 from '../src/Assets/images/icons/file-type.svg';
import SettingIcon10 from '../src/Assets/images/icons/help-square.svg';
import SettingIcon11 from '../src/Assets/images/icons/privacy-icon.png';




export const ImagePath = { 
    Logo:Logo,
    ProfileImage:ProfileImage,
    NavIcon1:NavIcon1,
    NavIcon2:NavIcon2,
    NavIcon3:NavIcon3,
    NavIcon4:NavIcon4,
    NavIcon5:NavIcon5,
    NavIcon6:NavIcon6,
    NavIcon7:NavIcon7,
    SearchIcon:SearchIcon,
    BellIcon:BellIcon,
    TipIcon:TipIcon,
    MailLogo1:MailLogo1,
    MailLogo2:MailLogo2,
    Star:Star,
    StarFill:StarFill,
    DemoImg:DemoImg,
    AvtarImg:AvtarImg,
    SettingIcon1:SettingIcon1,
    SettingIcon2:SettingIcon2,
    SettingIcon3:SettingIcon3,
    SettingIcon4:SettingIcon4,
    SettingIcon5:SettingIcon5,
    SettingIcon6:SettingIcon6,
    SettingIcon7:SettingIcon7,
    SettingIcon8:SettingIcon8,
    SettingIcon9:SettingIcon9,
    SettingIcon10:SettingIcon10,
    SettingIcon11:SettingIcon11,   

}